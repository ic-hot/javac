package ic.javac


import ic.struct.collection.Collection
import ic.system.funs.executeBashScript
import ic.text.*
import ic.base.throwables.CompilationException
import ic.base.throwables.NotNeededException
import ic.storage.fs.Directory
import ic.storage.fs.local.impl.funs.setFilePermissions
import ic.struct.sequence.ext.foreach.forEach


@Throws(CompilationException::class, NotNeededException::class)
fun compileJava(

	inputDir  : Directory,
	outputDir : Directory,

	compiledDependencyDirs : Collection<Directory>,
	jarDependencyDirs : Collection<Directory>,

	handleWarning : (String) -> Unit,

	javaVersion : String,

	javacPath : String = "javac",
	javacOptions : String = ""

) {

	val script = generateJavaCompileScript(
		inputDir = inputDir,
		outputDir = outputDir,
		compiledDependencies = compiledDependencyDirs,
		jarDependencies = jarDependencyDirs,
		javaVersion = javaVersion,
		javacPath = javacPath,
		javacOptions = javacOptions
	)

	val response = executeBashScript(script)

	SplitText(response, '\n').forEach { line ->
		when {

			line.isEmpty -> {}

			line.startsWith("Note: ")		-> handleWarning(line.toString())
			line.contains(": warning: ")	-> handleWarning(line.toString())
			line.startsWith("\t")			-> handleWarning(line.toString())
			line.startsWith("  ")			-> handleWarning(line.toString())
			line.endsWith(" warning")		-> handleWarning(line.toString())
			line.endsWith(" warnings")		-> handleWarning(line.toString())

			line.startsWith("javac: file not found: ") && line.endsWith("/**/*.java") -> throw NotNeededException
			line.startsWith("error: file not found: ") && line.endsWith("/**/*.java") -> throw NotNeededException

			else -> throw CompilationException("$response\nLINE: $line")

		}
	}

	setFilePermissions(outputDir.absolutePath, isRecursive = true)

}