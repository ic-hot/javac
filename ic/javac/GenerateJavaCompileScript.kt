package ic.javac


import ic.base.strings.ext.asText
import ic.cmd.bash.bashPureSingleQuote
import ic.storage.fs.Directory
import ic.struct.collection.Collection
import ic.struct.collection.ext.copy.copyConvertToList
import ic.struct.list.ext.concat
import ic.text.Text


fun generateJavaCompileScript(

	inputDir : Directory,
	outputDir : Directory,

	compiledDependencies : Collection<Directory>,
	jarDependencies : Collection<Directory>,

	javaVersion : String,

	javacPath : String = "javac",
	javacOptions : String = ""

) : Text = (

	"shopt -s globstar;" +

	"$javacPath " + (
		"-source " + javaVersion + " " +
		"-target " + javaVersion + " " +
		"-Xlint:unchecked " +
		"-Xlint:-options " + javacOptions + " " +
		"-classpath " + (
			compiledDependencies.copyConvertToList { bashPureSingleQuote(it.absolutePath) }.concat(separator = ':') +
			":" +
			jarDependencies.copyConvertToList { bashPureSingleQuote(it.absolutePath + "/*") }.concat(separator = ':')
		) + " " +
		"-sourcepath " + bashPureSingleQuote(inputDir.absolutePath) + " " +
		"-d " + bashPureSingleQuote(outputDir.absolutePath) + " " +
		bashPureSingleQuote(inputDir.absolutePath) + "/**/*.java"
	)

).asText