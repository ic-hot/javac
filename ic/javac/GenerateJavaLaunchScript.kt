package ic.javac


import ic.struct.collection.Collection
import ic.system.localcmd.LocalBashSession
import ic.util.text.charset.Charset
import ic.text.Text
import ic.text.TextBuffer
import ic.cmd.bash.bashSingleQuote
import ic.storage.fs.Directory
import ic.storage.fs.ext.writeFileReplacing
import ic.struct.collection.ext.copy.copyConvertToList
import ic.struct.collection.ext.foreach.breakableForEach
import ic.struct.list.ext.concat
import ic.struct.map.finite.FiniteMap
import ic.text.empty.EmptyText
import ic.text.ext.toByteSequence


fun generateJavaLaunchScript(

	distributionDirectory : Directory,
	isPortable : Boolean,

	classDirs : Collection<Directory>,
	jarDirs : Collection<Directory>,

	mainClassName : String,
	args : Text = EmptyText,
	environmentVariables : FiniteMap<String, String> = FiniteMap(),

	javaPath : String = "java",
	targetBaseDirectory : Directory,
	scriptName : String

) {

	val launchScript = TextBuffer()

	launchScript.writeLine("#!/bin/bash")

	if (isPortable) {
		launchScript.writeLine("if [ -z \"\$IC_DISTRIBUTION_PATH\" ]")
		launchScript.writeLine("then")
		launchScript.writeLine("	export IC_DISTRIBUTION_PATH=$(pwd)")
		launchScript.writeLine("fi")
	} else {
		launchScript.writeLine("export IC_DISTRIBUTION_PATH=${ bashSingleQuote(distributionDirectory.absolutePath) }")
	}

	fun directoryToPath (directory: Directory) : String {
		return "\$IC_DISTRIBUTION_PATH/${ distributionDirectory.relativizePath(directory.absolutePath) }"
	}

	environmentVariables.keys.breakableForEach { envKey ->
		launchScript.writeLine("export $envKey=${ environmentVariables[envKey]!! }")
	}

	launchScript.writeLine(
		"$javaPath " +
		(
			"-classpath " +
			classDirs.copyConvertToList { directoryToPath(it) }.concat(separator = ':') +
			":" +
			jarDirs.copyConvertToList { jarDir -> "\"" + directoryToPath(jarDir) + "/*\"" }.concat(separator = ':')
		) + " " +
		"-ea" + " " +
		mainClassName + " " +
		(if (args.isEmpty) "" else args.toString() + " ") +
		"$@"
	)

	targetBaseDirectory.writeFileReplacing(
		name = scriptName,
		bytes = launchScript.toByteSequence(Charset.defaultUnix)
	)

	LocalBashSession(targetBaseDirectory).executeCommand("chmod +x $scriptName")

}